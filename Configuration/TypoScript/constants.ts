
module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper {
    view {
        # cat=module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper/file; type=string; label=Path to template root (BE)
        templateRootPath = EXT:hny_bm_folderzipper/Resources/Private/Backend/Templates/
        # cat=module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper/file; type=string; label=Path to template partials (BE)
        partialRootPath = EXT:hny_bm_folderzipper/Resources/Private/Backend/Partials/
        # cat=module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper/file; type=string; label=Path to template layouts (BE)
        layoutRootPath = EXT:hny_bm_folderzipper/Resources/Private/Backend/Layouts/
    }
    persistence {
        # cat=module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper//a; type=string; label=Default storage PID
        storagePid =
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder