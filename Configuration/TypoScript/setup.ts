
# Module configuration
module.tx_hnybmfolderzipper_tools_hnybmfolderzipperhnybackendmodulefolderzipper {
    persistence {
        storagePid = {$module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper.persistence.storagePid}
    }
    view {
        templateRootPaths.0 = EXT:hny_bm_folderzipper/Resources/Private/Backend/Templates/
        templateRootPaths.1 = {$module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper.view.templateRootPath}
        partialRootPaths.0 = EXT:hny_bm_folderzipper/Resources/Private/Backend/Partials/
        partialRootPaths.1 = {$module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper.view.partialRootPath}
        layoutRootPaths.0 = EXT:hny_bm_folderzipper/Resources/Private/Backend/Layouts/
        layoutRootPaths.1 = {$module.tx_hnybmfolderzipper_hnybackendmodulefolderzipper.view.layoutRootPath}
    }
}

## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder