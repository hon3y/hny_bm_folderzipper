<?php
namespace HNY\HnyBmFolderzipper\Tests\Unit\Controller;

/**
 * Test case.
 */
class ModuleControllerTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HNY\HnyBmFolderzipper\Controller\ModuleController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\HNY\HnyBmFolderzipper\Controller\ModuleController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

}
