<?php
namespace HNY\HnyBmFolderzipper\Tests\Unit\Domain\Model;

/**
 * Test case.
 */
class ModuleTest extends \TYPO3\CMS\Core\Tests\UnitTestCase
{
    /**
     * @var \HNY\HnyBmFolderzipper\Domain\Model\Module
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = new \HNY\HnyBmFolderzipper\Domain\Model\Module();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function dummyTestToNotLeaveThisFileEmpty()
    {
        self::markTestIncomplete();
    }
}
