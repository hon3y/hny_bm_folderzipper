<?php
defined('TYPO3_MODE') || die('Access denied.');

call_user_func(
    function()
    {

        if (TYPO3_MODE === 'BE') {

            \TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerModule(
                'HNY.HnyBmFolderzipper',
                'tools', // Make module a submodule of 'tools'
                'hnybackendmodulefolderzipper', // Submodule key
                '', // Position
                [
                    'Module' => 'bundle, remove',
                    
                ],
                [
                    'access' => 'user,group',
                    'icon'   => 'EXT:hny_bm_folderzipper/Resources/Public/Icons/user_mod_hnybackendmodulefolderzipper.svg',
                    'labels' => 'LLL:EXT:hny_bm_folderzipper/Resources/Private/Language/locallang_hnybackendmodulefolderzipper.xlf',
                ]
            );

        }

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile('hny_bm_folderzipper', 'Configuration/TypoScript', 'hny_bm_folderzipper');

        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addLLrefForTCAdescr('tx_hnybmfolderzipper_domain_model_module', 'EXT:hny_bm_folderzipper/Resources/Private/Language/locallang_csh_tx_hnybmfolderzipper_domain_model_module.xlf');
        \TYPO3\CMS\Core\Utility\ExtensionManagementUtility::allowTableOnStandardPages('tx_hnybmfolderzipper_domain_model_module');

    }
);
## EXTENSION BUILDER DEFAULTS END TOKEN - Everything BEFORE this line is overwritten with the defaults of the extension builder