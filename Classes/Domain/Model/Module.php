<?php
namespace HNY\HnyBmFolderzipper\Domain\Model;

/***
 *
 * This file is part of the "hny_bm_folderzipper" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * Module
 */
class Module extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    }
