<?php
namespace HNY\HnyBmFolderzipper\Controller;

/***
 *
 * This file is part of the "hny_bm_folderzipper" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2018
 *
 ***/

/**
 * ModuleController
 */
class ModuleController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * moduleRepository
     *
     * @var \HNY\HnyBmFolderzipper\Domain\Repository\ModuleRepository
     * @inject
     */
    protected $moduleRepository = null;

    /**
     * action bundle
     *
     * @return void
     */
    public function bundleAction()
    {

    }

    /**
     * action remove
     *
     * @return void
     */
    public function removeAction()
    {

    }
}
